# Cricket Scoreboard
This is a cricket scoreboard which can be used to store previous matches score. A work in progress as of now

## What can it do as of now
- Start a game
- Can do toss
- Can calculate runs/wickets/extras/run rate
- Will store/read every previous score
- Can remove all the previous score

## Highest priority works that I want to complete
- Adding batsman's name
- Add runs and bowls to the batsman
- Adding bowler name
- Add runs and wickets to the bowlers
- Change strike on even runs (1s, 3s, 5s etc)
- Change strike after over ends (if the bowl is either dot ball, double, four, six etc)

## Miscellaneous
- Adding tournament mode (with various teams)
- Choose Man of the Match on the stats
- Make GUI version of this application

# whoami
I am anant, a 13 year old **self taught programmer**. So that's why to complete this project. I am always banned at stack overflow, got a negative ranking of 2, 7 like that only. So no one is there to help me lol. I am trying to finish my highest priority work. Can't think of the logic (cause we are new in this things)