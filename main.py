# Importing external modules
import os
import time

# Declaring some variables (some of them are temporary)
welcomeMsg = """\n\033[1mEnter which choice you want to use\033[0m
1. Read previous score
2. Add a new game
3. Remove previous saved score
4. Quit
"""
whoWonTheToss = ""
batOrBowl = ""
tossOption = ["heads", "tails"]


# Defining some functions
# Function for adding team players in list
def playerNameInTeam(teamName, teamPlayers, howManyPlayersPlaying):
    teamPlayers = []
    print()

    i = 0
    while(i != howManyPlayersPlaying):
        players = input(f"Enter the {i+1} player of {teamName}: ")
        teamPlayers.append(players)
        i = i+1


# Adding players name in file
def addingPlayerNameInFile(teamName, teamPlayers):
    file.write(f"Team Name: {teamName}\n")
    i = 0
    while (i != len(teamPlayers)):
        file.write(f"{i+1}: {teamPlayers[i]}\n")
        i += 1


# This function which will execute every inning
def Play(teamName):
    score = 0
    wickets = 0
    extras = 0
    currentBall = 1
    currentOver = 1
    howManyBalls = howManyOverTheMatchIs * 6

    print(f"\n\033[1mInnings of {teamName} has started\033[0m")

    while currentBall != howManyBalls:
        print("\n_______________________________")
        print(
            f"\033[1m\nStatus of {currentBall} ball (enter a number)\033[0m")
        print("""1. Runs
2. Dot ball
3. Wicket
4. Wide Ball/No Ball
5. Force Quit
""")
        while True:
            try:
                choice = int(input("Enter the choice here: "))
                break
            except:
                print("Enter a number only")

        if choice == 1:
            while True:
                try:
                    howManyRunsGot = int(
                        input("How many runs did the batsman scored\n"))
                    break
                except:
                    print("Enter a number only")
            score += howManyRunsGot
            currentBall += 1
            if currentBall % 6 == 0:
                print(f"\n{currentOver} over finished")
                currentOver += 1
        elif choice == 2:
            currentBall += 1
            if currentBall % 6 == 0:
                print(f"{currentOver} over finished")
                currentOver += 1
        elif choice == 3:
            currentBall += 1
            wickets += 1
            if currentBall % 6 == 0:
                print(f"{currentOver} over finished")
                currentOver += 1
            if wickets == howManyPlayersPlaying - 1:
                print("Sorry but you are all out")
                break
        elif choice == 4:
            extras += 1
            score += 1
        elif choice == 5:
            quit()
        else:
            print("Enter a valid value")

    print(f"""\n\033[1mTotal Score of {teamName}\033[0m
\033[1mRuns: {score}\033[0m
\033[1mWickets: {wickets}\033[0m
\033[1mExtras: {extras}\033[0m
\033[1mRun Rate: {score/howManyOverTheMatchIs}\033[0m\n""")

    file.write(f"""\nTotal Score of {teamName}\n)
Runs: {score}\n
Wickets: {wickets}\n
Extras: {extras}\n
Run Rate: {score/howManyOverTheMatchIs}\n""")


# If team 1 is batting first
def team1BatFirst(teamName1, teamName2):
    Play(teamName1)
    print("\033[1mInnings 1 finished\033[0m")
    Play(teamName2)


# If team 2 is batting first
def team2BatFirst(teamName1, teamName2):
    Play(teamName2)
    print("\033[1mInnings 1 finished\033[0m")
    Play(teamName1)


# Function which will start the match
def StartMatch(batOrBowl, teamName1, teamName2, whoWonTheToss):
    if whoWonTheToss == teamName1:
        if batOrBowl == "bat":
            team1BatFirst(teamName1, team2Name)
        else:
            team2BatFirst(teamName1, teamName2)
    elif whoWonTheToss == teamName2:
        if batOrBowl == "bat":
            team2BatFirst(teamName1, teamName2)
        else:
            team1BatFirst(teamName1, teamName2)


# For reading the previous score
def choice1():
    try:
        with open('cricket.txt') as file:
            file = open("cricket.txt", "r")
            print("\n\033[1mScore\033[0m\n")
            allTheContent = file.read()
            print(allTheContent)
            print("______________________________________\n")
    except:
        print("\n\033[1mNo score exists\033[0m")
        print("_______________")

# For removing the previous score


def choice3():
    try:
        os.remove("cricket.txt")
        print("\033[1mPrevious Scores Removed\033[0m")
    except:
        print("\n\033[1mNo score exists\033[0m")
        print("______________")


# Starting of program
while True:
    print("\033[1m\nWelcome to our Cricket Scoreboard software\033[0m")
    print(welcomeMsg)

    while True:
        try:
            choice = int(input("Add the number: "))
            break
        except:
            print("Enter a number only\n")
            print(welcomeMsg)

    if choice == 1:
        choice1()

    elif choice == 2:
        team1Name = input("Enter team 1 name: ")
        team2Name = input("Enter team 2 name: ")

        tossTeams = [team1Name, team2Name]

        print()

        # How many players are playing
        while True:
            try:
                howManyPlayersPlaying = int(
                    input("How many players are playing (from one team) in the match\n"))
                break
            except:
                print("Enter a number only\n")

        # Team 1 player
        team1Players = []
        playerNameInTeam(team1Name, team1Players, howManyPlayersPlaying)

        # Team 2 players
        team2Players = []
        playerNameInTeam(team2Name, team2Players, howManyPlayersPlaying)

        # Putting team 1 player name in file
        file = open("cricket.txt", "a")
        addingPlayerNameInFile(team1Name, team1Players)

        # Putting team 2 player name in file
        addingPlayerNameInFile(team1Name, team1Players)

        # Asking how many over the match is
        print("__________________________________\n")
        while True:
            try:
                howManyOverTheMatchIs = int(
                    input("Enter how many overs this match is\n"))
                break
            except:
                print("\nEnter a number only\n")

        # Putting how many over match in file
        while True:
            try:
                file.write(f"\nMatch overs: {howManyOverTheMatchIs}\n")
                break
            except:
                print("Enter a valid number")

        # Doing the toss
        print("\nNow do the toss")
        time.sleep(5)
        i = 1
        for team in tossTeams:
            print(f"{i}: {team}")
            i += 1

        while True:
            try:
                whoWonTheToss = int(input("Who won the toss (Enter number)? "))
                if whoWonTheToss != 1 and whoWonTheToss != 2:
                    print("Enter a valid number")
                else:
                    if whoWonTheToss == 1:
                        whoWonTheToss = team1Name
                    else:
                        whoWonTheToss = team2Name
                    break
            except:
                print("Enter a number only")

        # Asking for the decision (bat or bowl)
        while True:
            batOrBowl = input(
                f"\nEnter what {whoWonTheToss} want to do (bat/bowl): ")
            if (batOrBowl != "bat") and (batOrBowl != "bowl"):
                print("Enter a valid value")
            else:
                break

        file.write(
            f"{whoWonTheToss} won the toss and chosed to {batOrBowl} first\n")

        # Game
        StartMatch(batOrBowl, team1Name, team2Name, whoWonTheToss)
        file.close()

    # For Removing the previous scores
    elif choice == 3:
        choice3()

    # For exiting from the program
    elif choice == 4:
        print("Quitting")
        exit()

    # If option entered is not valid
    else:
        print("Enter a valid choice")
